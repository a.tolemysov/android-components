package ayupov.ilgam.lesson002;

import ayupov.ilgam.lesson002.model.Element;

public interface MainClickListener {

    void onClick(Element element);

}
