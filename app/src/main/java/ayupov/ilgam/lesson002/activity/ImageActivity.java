package ayupov.ilgam.lesson002.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import ayupov.ilgam.lesson002.R;

public class ImageActivity extends AppCompatActivity {
    public int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        final ImageView imageView = findViewById(R.id.image_view);
        Button button = findViewById(R.id.image_size);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0) {
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    count++;
                } else if (count == 1) {
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    count++;
                } else if (count == 2) {
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    count++;
                } else if (count == 3) {
                    imageView.setScaleType(ImageView.ScaleType.FIT_END);
                    count++;
                } else if (count == 4) {
                    imageView.setScaleType(ImageView.ScaleType.FIT_START);
                    count++;
                } else if (count == 5) {
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    count++;
                } else if (count == 6) {
                    imageView.setScaleType(ImageView.ScaleType.MATRIX);
                    count = 0;
                }
            }
        });
    }
}
