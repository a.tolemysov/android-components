package ayupov.ilgam.lesson002.activity;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.DrawableUtils;

import ayupov.ilgam.lesson002.R;

public class SwitchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch);



        Switch switchButton = findViewById(R.id.switch_button);
        final LinearLayout linearLayout = findViewById(R.id.switch_layout);
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    linearLayout.setBackgroundColor(Color.BLACK);
                } else {
                    linearLayout.setBackgroundColor(Color.WHITE);
                }
            }
        });
    }
}
