package ayupov.ilgam.lesson002.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ayupov.ilgam.lesson002.model.Element;
import ayupov.ilgam.lesson002.MainAdapter;
import ayupov.ilgam.lesson002.MainClickListener;
import ayupov.ilgam.lesson002.R;

import static ayupov.ilgam.lesson002.ElementsBuilder.BUTTON_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.CHECK_BOX_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.EDIT_TEXT_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.IMAGE_VIEW_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.PROGRESS_BAR_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.RADIO_GROUP_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.RATING_BAR_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.SWITCH_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.TEXT_VIEW_ID;
import static ayupov.ilgam.lesson002.ElementsBuilder.buildElements;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        MainAdapter mainAdapter = new MainAdapter(buildElements(), new MainClickListener() {
            @Override
            public void onClick(Element element) {
                switch (element.getId()) {
                    case BUTTON_ID:
                        startActivity(new Intent(MainActivity.this, ButtonActivity.class));
                        break;
                    case TEXT_VIEW_ID:
                        startActivity(new Intent(MainActivity.this, TextViewActivity.class));
                        break;
                    case EDIT_TEXT_ID:
                        startActivity(new Intent(MainActivity.this, EditTextActivity.class));
                        break;
                    case CHECK_BOX_ID:
                        startActivity(new Intent(MainActivity.this, CheckBoxActivity.class));
                        break;
                    case RADIO_GROUP_ID:
                        startActivity(new Intent(MainActivity.this, RadioActivity.class));
                        break;
                    case SWITCH_ID:
                        startActivity(new Intent(MainActivity.this, SwitchActivity.class));
                        break;
                    case IMAGE_VIEW_ID:
                        startActivity(new Intent(MainActivity.this, ImageActivity.class));
                        break;
                    case PROGRESS_BAR_ID:
                        startActivity(new Intent(MainActivity.this, ProgressBarActivity.class));
                        break;
                    case RATING_BAR_ID:
                        startActivity(new Intent(MainActivity.this, RatingActivity.class));
                        break;
                    default:
                        Toast.makeText(MainActivity.this, element.getName(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mainAdapter);
    }
}
