package ayupov.ilgam.lesson002.activity;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import ayupov.ilgam.lesson002.R;

public class TextViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);

        final TextView textView = findViewById(R.id.text_view);

        findViewById(R.id.text_view_black).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTextColor(Color.BLACK);
            }
        });

        findViewById(R.id.text_view_blue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTextColor(Color.BLUE);
            }
        });

        findViewById(R.id.text_view_green).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTextColor(Color.GREEN);
            }
        });

        findViewById(R.id.text_view_small).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTextSize(12);
            }
        });

        findViewById(R.id.text_view_middle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTextSize(17);
            }
        });

        findViewById(R.id.text_view_big).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTextSize(22);
            }
        });

        findViewById(R.id.text_view_normal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTypeface(Typeface.DEFAULT);
            }
        });

        findViewById(R.id.text_view_bold).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            }
        });

        findViewById(R.id.text_view_italic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
            }
        });
    }
}
